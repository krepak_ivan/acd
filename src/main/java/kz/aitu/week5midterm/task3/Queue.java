package kz.aitu.week5midterm.task3;

public class Queue {
    QueueNode head;
    QueueNode tail;
    int i = 0;

    public Queue(){
    }

    public QueueNode head() {
        return head;
    }

    public QueueNode tail() {
        return tail;
    }

    public void push(String s){

        QueueNode node = new QueueNode(s);
        if(head==null){
            head=node;
            tail=node;
        }

        else{

            tail.next=node;
            tail=node;
        }
        i++;
    }
    public QueueNode peek(){
        return head;
    }

    public QueueNode poll(){
        QueueNode acd=head;
        head = head.next;
        i=i-1;
        return acd;
    }

    public int size(){
        return i;
    }

    public QueueNode pop(){

        QueueNode temp = tail;
        QueueNode acd = head;
        while(acd.next.next != null)
        {
            acd=acd.next;

        }
        i=i-1;
        tail=acd;
        tail.next = null;
        return temp;
    }
    public boolean empty(){
        if(head==null){
            return true;
        }
        else{
            return false;
        }
    }
    public static void printOdd(QueueNode node){
        int j=0;
        if(node==null)return;
        int k = node.data.length();
        if(k%2==1){
            System.out.println(node);
        }
        printOdd(node.next);
    }

}
