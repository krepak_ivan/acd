package kz.aitu.week5midterm.task1;

public class LinkedListTest {
    public static void main(String[] args) {
        LinkedList ls = new LinkedList();
        Node head = ls.head();

        ls.add(new Node("Data1"));
        ls.add(new Node("Data2"));
        ls.add(new Node("Data3"));
        ls.add(new Node("Data4"));
        ls.add(new Node("Data5"));
        ls.add(new Node("Data6"));
        ls.add(new Node("Data7"));

        ls.push_back("newData");//added element

        Node b = ls.head();
        ls.printRecursively(b);
    }


}
