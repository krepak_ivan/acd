package kz.aitu.week5midterm.task1;

public class LinkedList {
    public Node head;
    public Node tail;

    public LinkedList(){
        this.head = new Node("head");
        tail = head;
    }

    public Node head() {
        return head;
    }

    public void add(Node node) {
        tail.next = node;
        tail = node;
    }

    public void push_back (String str) { //O(n)
        Node temp = new Node(str);
        tail.setNext(temp);
        tail = temp;
    }

    public void printRecursively(Node node) { //O(n)
        if(node == null) return;

        if(node != null){
            System.out.print(node.getData() + " ");
            node = node.getNext();
            printRecursively(node);
        }
    }
}
