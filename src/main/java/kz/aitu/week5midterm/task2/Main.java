package kz.aitu.week5midterm.task2;

import java.util.*;

public class Main {
    static Stack<Integer> res = new Stack<Integer>();
    static Stack<Integer> tmpStack = new Stack<Integer>();

    public static void main(String args[]){
        Stack<Integer> s1 = new Stack<Integer>();
        Stack<Integer> s2 = new Stack<Integer>();
        s1.push(1);
        s1.push(2);
        s1.push(3);
        s1.push(4);
        s1.push(5);

        s2.push(2);
        s2.push(6);
        s2.push(7);
        s2.push(4);
        s2.push(9);
        s2.push(11);

        merege(s1, s2);

        while (tmpStack.size() != 0) {
            System.out.print(tmpStack.peek() + " ");
            tmpStack.pop();
        }
    }

    static void sortStack(Stack<Integer> input){
        while (input.size() != 0) {
            int tmp = input.peek();
            input.pop();

            while (tmpStack.size() != 0 && tmpStack.peek() > tmp) {
                input.push(tmpStack.peek());
                tmpStack.pop();
            }

            tmpStack.push(tmp);
        }
    }

    static void merege(Stack<Integer> s1, Stack<Integer> s2) {
        while (s1.size() != 0) {
            res.push(s1.peek());
            s1.pop();
        }

        while (s2.size() != 0) {
            res.push(s2.peek());
            s2.pop();
        }

        sortStack(res);
    }
}
