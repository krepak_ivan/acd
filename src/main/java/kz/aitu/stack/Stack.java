package kz.aitu.stack;

public class Stack {
   Node top;

    public Node getTop() {
        return top;
    }

    public void setTop(Node top) {
        this.top = top;
    }

    public int pop() {
        Node popped = top;
        top = top.nextNode;
        return popped.data;
    }

    public void push(int data) {
        Node pushed = new Node(data);
        if(top == null){
            top = pushed;
        } else {
            pushed.nextNode = top;
            top = pushed;
        }
    }

    public int size(){
        int counter = 0;
        Node current = top;
        if(top==null){
            return 0;
        }else{
            while(current != null){
                current = current.nextNode;
                counter++;
            }
        }
        return counter;
    }

    public boolean stackIsEmpty(){
        if(top == null) return true;
        else return false;
    }
    public void top(int data){
        Node pushed = new Node(data);
        System.out.println(data);
    }
    public int top(){
        if (top == null){
            return 0;
        }else{
            return top.data;
        }
    }

}
