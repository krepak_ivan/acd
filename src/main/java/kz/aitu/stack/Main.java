package kz.aitu.stack;

public class Main {
    public static void main(String[] args) {
        Stack s = new Stack();
        for(int q = 1; q<=10; q++){
            s.push(q);
        }

        System.out.println("Printing stack before changes");
        Node current = s.top;
        while(current != null){
            System.out.print(current.data + " ");
            current = current.nextNode;
        }

        System.out.println("Popped is: " + s.pop());
        System.out.println("Is empty ?: " + s.stackIsEmpty());
        System.out.println("Size is: " + s.size());

        System.out.println("Stack after changes");
        Node currentNode = s.top;
        while(currentNode != null){
            System.out.print(currentNode.data + " ");
            currentNode = currentNode.nextNode;
        }
    }
}
