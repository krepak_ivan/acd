package kz.aitu.week6.bst_points;

public class Node {
    private int key;
    private String name;
    private Node left;
    private Node right;
    private int score;

    public Node(int key, String name, int score){
        this.key = key;
        this.name = name;
        this.score = score;
        this.left = null;
        this.right = null;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public Node getLeft() {
        return left;
    }

    public int getKey() {
        return key;
    }

    public int getScore() {
        return score;
    }

    public String getName() {
        return name;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
