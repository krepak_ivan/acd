package kz.aitu.week6.bst_points;

public class Main {
    public static void main (String[] args) {
        BSTree studentsTree = new BSTree();
        studentsTree.addRoot(21, "Olki");

        studentsTree.addRoot(16, "Ivan");
        studentsTree.addRoot(9, "Zhasya");
        studentsTree.addRoot(19, "Almaz");
        studentsTree.addRoot(6, "Venera");
        studentsTree.addRoot(13, "Alikhan");
        studentsTree.addRoot(12, "Alisher Zh");
        studentsTree.addRoot(18, "Tima");
        studentsTree.addRoot(3, "Alisher A");
        studentsTree.addRoot(11, "Aslan");
        studentsTree.addRoot(1, "Ardaqtym");
        studentsTree.addRoot(10, "Milena");
        studentsTree.addRoot(15, "Ayan");
        studentsTree.addRoot(14, "Dimash");
        studentsTree.addRoot(8, "Aybek");
        studentsTree.addRoot(5, "Dias");
        studentsTree.addRoot(17, "Azat");
        studentsTree.addRoot(2, "Diana");
        studentsTree.addRoot(7, "Beka");
        studentsTree.addRoot(20, "Daulet");
        studentsTree.addRoot(4, "Sabina");

        studentsTree.printAll();
        System.out.println();

        System.out.println("Printing Aslan's and Dias' marks: ");
        studentsTree.studentScore(11);
        studentsTree.studentScore(5);

        System.out.println("Printing all student's marks: ");
        studentsTree.studentScoreAll();
    }
}