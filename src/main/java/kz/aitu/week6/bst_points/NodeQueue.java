package kz.aitu.week6.bst_points;

public class NodeQueue {
    private int key;
    private NodeQueue next;

    public NodeQueue(int key){
        this.key = key;
    }

    public int getKey(){
        return key;
    }

    public NodeQueue getNext(){
        return next;
    }

    public void setNext(NodeQueue next){
        this.next = next;
    }
}
