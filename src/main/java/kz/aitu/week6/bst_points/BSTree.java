package kz.aitu.week6.bst_points;

//import kz.aitu.stack.Stack;

public class BSTree {
    private Node root;

    private int totalSum = 0;
    private static int counter = 0;

    public BSTree() {
        this.root = null;
    }

    public void addRoot(int key, String name){
        Node currentNode = root;
        Node parentNode = null;

        if(root == null) root = new Node(key, name, 0);
        else{
            while(currentNode != null){
                if(key <= currentNode.getKey()){
                    parentNode = currentNode;
                    currentNode = currentNode.getLeft();
                }else{
                    parentNode = currentNode;
                    currentNode = currentNode.getRight();
                }
            }

            if(passedOrNot(parentNode)){
                if(key <= parentNode.getKey()){
                    parentNode.setLeft(new Node(key, name, 0));
                }else{
                    parentNode.setRight(new Node(key, name, 0));
                }
            }
        }

        counter++;
        studentPassed(key);
    }

    private boolean passedOrNot(Node parentNode){
        if(parentNode.getScore() == 0) return false;
        else return true;
    }

    public void studentPassed(int key){
        Node currentNode = root;

        while(currentNode != null){
            if(key < currentNode.getKey()) currentNode = currentNode.getLeft();
            else if(key > currentNode.getKey()) currentNode = currentNode.getRight();
            else{
                currentNode.setScore(80);
                break;
            }
        }
    }

    public void printAll(){
        Queue queueToPrint = new Queue();
        queueToPrint.push(root.getKey());
        printQueue(root, queueToPrint);
    }

    private void printQueue(Node root, Queue tempQueue){
        while(!tempQueue.isEmpty()){
            Node currentNode = root;
            int points = printTempQueue(tempQueue);

            while(currentNode != null){
                if (points < currentNode.getKey()) currentNode = currentNode.getLeft();
                else if(points > currentNode.getKey()) currentNode = currentNode.getRight();
                else if (points == currentNode.getKey()) break;
            }

            pushQueue(currentNode.getLeft(), tempQueue);
            pushQueue(currentNode.getRight(), tempQueue);
        }
    }

    private int printTempQueue(Queue tempQueue){
        int newRoot = tempQueue.pop();
        Node currentNode = root;

        while(currentNode != null){
            if(newRoot < currentNode.getKey()) currentNode = currentNode.getLeft();
            else if(newRoot > currentNode.getKey()) currentNode = currentNode.getRight();
            else{
                System.out.print(currentNode.getName() + " ");
                break;
            }
        }

        return newRoot;
    }

    private void pushQueue(Node root, Queue tempQueue){
        if (root != null) tempQueue.push(root.getKey());
    }

    public void studentScore(int key){
        Node currentNode = root;

        if(key == currentNode.getKey()) returnScoreIfParent(currentNode);
        else{
            while(currentNode != null){
                if(key < currentNode.getKey()) currentNode = currentNode.getLeft();
                else if(key > currentNode.getKey()) currentNode = currentNode.getRight();
                else break;
            }

            returnScoreIfChild(currentNode);
        }

        System.out.println(currentNode.getName() + " " + totalSum);
        totalSum = 0;
    }

    public void studentScoreAll(){
        scoresOfAllPrintNow(counter);
    }

    public void scoresOfAllPrintNow(int counter){
        Node currentNode = root;

        if(counter == currentNode.getKey()) returnScoreIfParent(currentNode);
        else{
            while(currentNode != null){
                if(counter < currentNode.getKey()) currentNode = currentNode.getLeft();
                else if(counter > currentNode.getKey()) currentNode = currentNode.getRight();
                else break;
            }

            returnScoreIfChild(currentNode);
        }

        System.out.println(currentNode.getName() + " " + totalSum);
        totalSum = 0;
        counter--;

        if(counter > 0) scoresOfAllPrintNow(counter);
    }

    private void returnScoreIfParent(Node root){
        Queue queue = new Queue();
        queue.push(root.getKey());
        checkScoreIfRoot(root, queue);
    }

    private void returnScoreIfChild(Node root){
        Queue queue = new Queue();
        queue.push(root.getKey());
        checkScoreIfChild(root, queue);
    }

    private void checkScoreIfRoot(Node root, Queue queue){
        while(!queue.isEmpty()){
            Node currentNode = root;
            int tempScore = queuePrintChild(queue, root);

            while(currentNode != null){
                if(tempScore < currentNode.getKey()) currentNode = currentNode.getLeft();
                else if(tempScore > currentNode.getKey()) currentNode = currentNode.getRight();
                else if(tempScore == currentNode.getKey()) break;
            }

            pushQueueChild(currentNode.getLeft(), queue);
            pushQueueChild(currentNode.getRight(), queue);
        }
    }

    private void checkScoreIfChild(Node root, Queue queue){
        while(!queue.isEmpty()){
            Node currentNode = root;
            int tempScore = queuePrintChildChild(queue, root);

            while(currentNode != null){
                if(tempScore < currentNode.getKey()) currentNode = currentNode.getLeft();
                else if (tempScore > currentNode.getKey()) currentNode = currentNode.getRight();
                else if (tempScore == currentNode.getKey()) break;
            }

            pushQueueChild(currentNode.getLeft(), queue);
            pushQueueChild(currentNode.getRight(), queue);
        }
    }

    private void pushQueueChild(Node root, Queue queue){
        if(root != null && root.getScore() != 0) queue.push(root.getKey());
    }

    private int queuePrintChild(Queue queue, Node rootNode){
        Node currentNode = root;
        int littleRoot = queue.pop();
        int tempScore = 7;

        if(currentNode.getKey() == littleRoot) totalSum = currentNode.getScore();
        else{
            while(currentNode != null) {
                if(littleRoot < currentNode.getKey()){
                    currentNode = currentNode.getLeft();

                    if(tempScore != 1) tempScore = tempScore - 2;
                }else if(littleRoot > currentNode.getKey()){
                    currentNode = currentNode.getRight();
                    if(tempScore != 1) tempScore = tempScore - 2;
                }else{
                    if(currentNode == rootNode) totalSum = currentNode.getScore();
                    break;
                }
            }

            totalSum = totalSum + tempScore;
        }

        return littleRoot;
    }


    private int queuePrintChildChild(Queue queue, Node rootNode){
        Node currentNode = rootNode;
        int littleRoot = queue.pop();
        int tempScore = 7;

        if(currentNode.getKey() == littleRoot) totalSum = currentNode.getScore();
        else{
            while(currentNode != null){
                if(littleRoot < currentNode.getKey()){
                    currentNode = currentNode.getLeft();

                    if(tempScore != 1) tempScore = tempScore - 2;
                }else if(littleRoot > currentNode.getKey()){
                    currentNode = currentNode.getRight();

                    if(tempScore != 1) tempScore = tempScore - 2;
                }else break;
            }

            totalSum = totalSum + tempScore;
        }

        return littleRoot;
    }
}