package kz.aitu.week6.bst_points;

public class Queue {
    private NodeQueue head;
    private NodeQueue tail;

    public void push(int key){
        NodeQueue newNodeQueue = new NodeQueue(key);
        if (head ==null){
            head = newNodeQueue;
            tail = newNodeQueue;
        } else {
            tail.setNext(newNodeQueue);
            tail = newNodeQueue;
        }
    }

    public int pop(){
        NodeQueue current = head;
        head = head.getNext();
        return current.getKey();
    }

    public boolean isEmpty(){
        return head == null;
    }
}
