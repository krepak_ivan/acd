package kz.aitu.week6.tree;

public class Main {
    public static void main(String[] args) {
        Tree t1 = new Tree();

        t1.insertValue(1);
        t1.insertValue(2);
        t1.insertValue(3);
        t1.insertValue(4);
        t1.insertValue(5);
        t1.insertValue(6);

        System.out.print(t1.toString());
    }
}
