package kz.aitu.week6.tree.sorting;

public class MergeSort {
    public void mergeSort(int array[], int left, int right) {
        int size = array.length;

        if(size == 0) return;

        if(left < right){
            int mid = (right + left) / 2;

            mergeSort(array, left, mid);
            mergeSort(array, mid + 1, right);
            merge(array, left, mid, right);
        }
    }

    public void merge(int array[], int left, int mid, int right){
        int left1 = mid - left + 1;
        int right1 = right - mid;

        int array1[] = new int[left1];
        int array2[] = new int[right1];

        for (int i = 0; i < left1; i++) array1[i] = array[left + i];
        for (int j = 0; j < right1; j++) array2[j] = array[mid + 1 + j];

        int i = 0;
        int j = 0;

        int k = left;

        while(i < left1 && j < right1){
            if(array1[i] > array2[j]){
                array[k] = array2[j];
                j++;
            }else{
                array[k] = array1[i];
                i++;
            }

            k++;
        }

        while(i < left1){
            array[k] = array1[i];
            i++;
            k++;
        }

        while(j < right1) {
            array[k] = array2[j];
            j++;
            k++;
        }
    }
}