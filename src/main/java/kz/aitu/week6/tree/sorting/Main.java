package kz.aitu.week6.tree.sorting;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int[] array = new int[10];
        inputData(array);
        int[] arraySelectionSort = new int[10];
        inputData(arraySelectionSort);
        int[] arrayInsertionSort = new int[10];
        inputData(arrayInsertionSort);

        int[] arrayMergeSort = new int[10];
        inputData(arrayMergeSort);

        int[] arrayQuickSort = new int[10];
        inputData(arrayQuickSort);
        int[] arrayBubbleSort = new int[10];
        inputData(arrayBubbleSort);

        BubbleSort bubble_sort = new BubbleSort();
        SelectionSort selection_sort = new SelectionSort();
        InsertionSort insertion_sort = new InsertionSort();
        MergeSort merge_sort = new MergeSort();
        QuickSort quick_sort = new QuickSort();

        int size = 10;

        System.out.print("Printing unsorted array : ");
        for(int i = 0; i < size; i++){
            System.out.print(array[i] + " ");
        }

        System.out.println();
        System.out.print("Bubble sort: ");
        bubble_sort.bubbleSort(arrayBubbleSort);
        for(int i = 0; i < size; i++){
            System.out.print(arrayBubbleSort[i] + " ");
        }

        System.out.println();
        System.out.print("SelectionSort: ");
        selection_sort.selectionSort(arraySelectionSort);
        for(int i = 0; i < size; i++){
            System.out.print(arraySelectionSort[i] + " ");
        }

        System.out.println();
        System.out.print("InsertionSort: ");
        insertion_sort.insertionSort(arrayInsertionSort);
        for(int i = 0; i < size; i++){
            System.out.print(arrayInsertionSort[i] + " ");
        }

        System.out.println();
        System.out.print("MergeSort: "); //send 0 and size-1
        merge_sort.mergeSort(arrayMergeSort, 0, arrayMergeSort.length - 1);
        for(int i = 0; i < size; i++){
            System.out.print(arrayMergeSort[i] + " ");
        }

        System.out.println();
        System.out.print("QuickSort: ");
        quick_sort.quickSortStart(arrayQuickSort);
        for(int i = 0; i < size; i++){
            System.out.print(arrayQuickSort[i] + " ");
        }
    }
    private static void inputData(int[] array) {
        Random r = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] =  r.nextInt(10);
        }
    }
}