package kz.aitu.week6.tree.sorting;

public class QuickSort{
    public static void quickSortStart(int[] qArray){
        int size = qArray.length - 1;
        quickSort(qArray, 0, size);
    }

    private static void quickSort(int[] qArray, int first, int last){
        if(first < last){
            int j = first - 1;

            for(int i = first; i < last; i++){
                if(qArray[i] < qArray[last]){
                    j = j + 1;
                    int temp = qArray[j];
                    qArray[j] = qArray[i];
                    qArray[i] = temp;
                }
            }

            j = j + 1;
            int temp = qArray[j];
            qArray[j] = qArray[last];
            qArray[last] = temp;

            quickSort(qArray, first, j-1);
            quickSort(qArray, j+1, last);
        }else return;
    }
}