package kz.aitu.week6.tree.sorting;

public class InsertionSort {
    public static void insertionSort(int[] array){
        int size = array.length;

        for(int i = 1; i < size; i++){
            for(int j = 0; j < i; j++){
                if(array[i] <= array[j]){
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
    }
}
