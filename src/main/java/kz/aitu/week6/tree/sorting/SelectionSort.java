package kz.aitu.week6.tree.sorting;

public class SelectionSort {
    public static void selectionSort(int[] array){
        int size = array.length;

        for(int i = 0; i < size; i++){
            int min = i;

            for(int j = i; j < size; j++){
                if(array[j] <= array[min]) min=j;
            }

            int temp = array[i];
            array[i] = array[min];
            array[min] = temp;
        }
    }
}
