package kz.aitu.week6.tree;

public class Tree {
    Node rootNode;

    public void insertNode(Node newNode){
        if (rootNode == null) rootNode = new Node(newNode.getValue());
        else insertNode(rootNode, newNode);
    }

    private void insertNode(Node currentNode, Node newNode){
        if(newNode.getValue() < currentNode.getValue()) {
            if (currentNode.getLeftNode() == null) currentNode.setLeftNode(newNode);
            else insertNode(currentNode.getLeftNode(), newNode);
        }

        if(newNode.getValue() > currentNode.getValue()) {
            if (currentNode.getRightNode() == null) currentNode.setRightNode(newNode);
            else insertNode(currentNode.getRightNode(), newNode);
        }
    }

    public void insertValue(int value) {
        insertNode(new Node(value));
    }

    public Node getNode(Node currentNode, int value){
        if(currentNode == null) return null;
        if(currentNode.getValue() == value) return currentNode;
        if(value < currentNode.getValue()) return getNode(currentNode.getLeftNode(), value);

        return getNode(currentNode.getRightNode(), value);
    }

    public String toString(){
        if (rootNode != null){
            return rootNode.toString();
        }

        return "";
    }
}