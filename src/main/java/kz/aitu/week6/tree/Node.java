package kz.aitu.week6.tree;

public class Node {
    int value;
    Node leftNode;
    Node rightNode;

    public Node(int value){
        leftNode = null;
        rightNode = null;
        this.value = value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setLeftNode(Node leftNode) {
        this.leftNode = leftNode;
    }

    public void setRightNode(Node rightNode) {
        this.rightNode = rightNode;
    }

    public int getValue() {
        return value;
    }

    public Node getLeftNode() {
        return leftNode;
    }

    public Node getRightNode() {
        return rightNode;
    }

    public String toString() {
        String result = value + "";
        if(leftNode  != null) result = leftNode.toString() + " " + result;
        if(rightNode != null) result = result + " " + rightNode.toString();

        return result;
    }
}
