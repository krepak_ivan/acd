package kz.aitu.ushinshi_toqsan.employee_task;

public class Main {
    public static void main(String[] args) {
        Employee employee_1 = new Employee();
        Employee employee_2 = new Employee();
        Employee employee_3 = new Employee();

        employee_1.setRate(10);
        employee_1.setHours(11);
        employee_1.setName("Tleuzhan");

        employee_2.setRate(20);
        employee_2.setHours(12);
        employee_2.setName("Venera");

        employee_3.setRate(30);
        employee_3.setHours(13);
        employee_3.setName("Alisher");

        employee_1.salary("Tleuzhan");
        employee_1.toString("Tleuzhan");
        employee_1.changeRate("Tleuzhan", 20);
        employee_1.bonuses("Tleuzhan");

        System.out.println("1st employee's name is " + employee_1.getName());
        employee_1.salary();
        employee_1.toString();
        employee_1.changeRate(30);
        employee_1.bonuses();

        System.out.println("2nd employee's name is " + employee_2.getName());
        employee_2.salary();
        employee_2.toString();
        employee_2.changeRate(31);
        employee_2.bonuses();

        System.out.println("3rd employee's name is ");
        employee_3.salary();
        employee_3.toString();
        employee_3.changeRate(32);
        employee_3.bonuses();

        System.out.println("Hours of all employees");
        System.out.println("Hours of 1st employee is");
        System.out.println(employee_1.getName() + " - " + employee_1.getHours());

        System.out.println("Hours of 2nd employee is");
        System.out.println(employee_2.getName() + " - " + employee_2.getHours());

        System.out.println("Hours of 3rd employee is");
        System.out.println(employee_3.getName() + " - " + employee_3.getHours());
    }
}
