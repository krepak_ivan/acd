package kz.aitu.threestacks;

public class Main {
    public static void main(String[] args) {
        Stack s1 = new Stack();
        Stack s2 = new Stack();
        Stack s3 = new Stack();

        s1.push(99);
        //s1.push(1);
        s1.push(2);
        s1.push(3);
//        s1.push(17);
//        s1.push(2);
//        s1.push(1);
//        s1.push(4);

        s2.push(5);
        s2.push(6);
        s2.push(7);
//        s2.push(3);
//        s2.push(4);
//        s2.push(4);
//        s2.push(17);

        s3.push(8);
        s3.push(9);
        s3.push(10);
//        s3.push(30);
//        s3.push(20);
//        s3.push(5);
//        s3.push(6);
//        s3.push(10);

        Stack s4 = new Stack();
        s4.top = mergeStacks(s1.top, s2.top, s3.top);

        System.out.println("Stack values are: ");

        Node thisNode = s4.top;
        while(thisNode != null){
            System.out.print(thisNode.value + " ");
            thisNode = thisNode.next;
        }
    }

    public static Node mergeStacks(Node s1head, Node s2head, Node s3head){
        Node temp = new Node(1);
        Node tail = temp;

        while(true){
            if(s1head == null && s2head == null){
                tail.next = s3head;
                break;
            }
            if(s2head == null && s3head == null){
                tail.next = s1head;
                break;
            }
            if(s3head == null && s1head == null){
                tail.next = s2head;
                break;
            }

            if(s1head.value <= s2head.value && s1head.value <= s3head.value){
                tail.next = s1head;
                s1head = s1head.next;
            }else if(s2head.value <= s1head.value && s2head.value <= s3head.value){
                tail.next = s2head;
                s2head = s2head.next;
            }else{
                tail.next = s3head;
                s3head = s3head.next;
            }

            tail = tail.next;
        }
        return temp.next;
    }
}