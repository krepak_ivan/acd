package kz.aitu.threestacks;

public class Stack {
    Node top;
    Node tail;

    public Stack(){
        this.top = new Node(0);
    }

    public Node top(){
        return top;
    }

    public void pop(){
        Node pop = top;
        top = top.next;
    }

    public void push(int value){
        Node tempNode = new Node(value);

        if(top == null){
            top = tempNode;
            tail = tempNode;
        }else{
            tempNode.next = top;
            top = tempNode;

        }
    }
}
