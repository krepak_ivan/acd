package kz.aitu.quizone;

import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class task1 {

    public static void main(String[] args) throws UnsupportedEncodingException {
        int N;
        Scanner input = new Scanner(System.in);
        N = input.nextInt();
        fun1(N);
    }

    static int n = 1;

    static void fun1(int N) {
        if (n <= N) {
            System.out.println(n);
            n++;
            fun1(N);
        } else {
            return;
        }
    }

//    static void fun2(int N) {
//        if (n <= N) {
//            System.out.println(n);
//            n++;
//            fun1(N);
//        } else {
//            return;
//        }
//    }
}
