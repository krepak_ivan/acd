package kz.aitu.quizone;

import java.util.Scanner;

public class task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String word;
        word = input.nextLine();

        System.out.println(recursionFunction(word));
    }

    public static String recursionFunction(String word){
        if(word.length() == 1){
            System.out.println("YES");
        }
        if(word.substring(0, 1).equals(word.substring(word.length() - 1, word.length()))){
            if(word.length() == 2){
                return "YES";
            }
            return recursionFunction(word.substring(1, word.length() - 1));
        }else{
            return "NO";
        }
    }
}