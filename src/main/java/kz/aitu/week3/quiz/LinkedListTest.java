package kz.aitu.week3.quiz;

public class LinkedListTest {
    public static void main(String args[]) {
        //creating LinkedList with 5 elements including head
        LinkedList linkedList = new LinkedList();
        Node head = linkedList.head();
        linkedList.add( new Node("1"));
        linkedList.add( new Node("2"));
        linkedList.add( new Node("3"));
        linkedList.add( new Node("4"));

        //finding middle element of LinkedList in single pass
        String middle = null; //write your code here - start
        int length = 0;

        Node nowValue = linkedList.head();
        while(nowValue != null){
            nowValue = nowValue.next();
            length = length + 1;
        }

        Node nowValueTwo = linkedList.head();
        int count = 0;
        if (length % 2 == 1){
            while(nowValueTwo != null){
                if(count == length / 2){
                    middle = nowValueTwo.data();
                    break;
                }
                nowValueTwo = nowValueTwo.next();
                count++;
            }
        } else {
            while(nowValueTwo != null){
                if(count == (length / 2) - 1){
                    middle = nowValueTwo.data();
                    break;
                }
            }
            nowValueTwo = nowValueTwo.next();
            count++;
        }

        //finish here

        System.out.println("length of LinkedList: " + length);
        System.out.println("middle element of LinkedList : " + middle);

    }
}
