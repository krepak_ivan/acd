package kz.aitu.week3.linkedlist;

public class Main {
    public static void main(String[] args) {
        Node n1 = new Node();
        Node n2 = new Node();
        Node n3 = new Node();
        Node n4 = new Node();
        Node n5 = new Node();
        Node n6 = new Node();

        n1.setData("Apple");
        n2.setData("Lemon");
        n3.setData("Qwerty");
        n4.setData("Keyboard");
        n5.setData("Lesson");
        n6.setData("English");

        n1.setNext(n2);
        n2.setNext(n3);
        n3.setNext(n4);
        n4.setNext(n5);
        n5.setNext(n6);
        n6.setNext(null);

        Node current = n1;
        while(current != null){
            System.out.print(current.getData() + " ");
            current = current.getNext();
        }
        System.out.println();

        Node current2 = n1;
        int size = 0;
        while(current2 != null){
            size++;
            current2 = current2.getNext();
        }
        System.out.println(size);

        Node n7 = new Node();
        n7.setData("iphone");

        n2.setNext(n7);
        n7.setNext(n3);
        Node current3 = n1;
        while(current3 != null){
            System.out.print(current3.getData() + " ");
            current3 = current3.getNext();
        }
        System.out.println();

        Node n8 = new Node();
        n8.setData("eight");
        n8.setNext(n1);
        Node current4 = n8;
        while(current4 != null){
            System.out.print(current4.getData() + " ");
            current4 = current4.getNext();
        }
        System.out.println();

        Node n9 = new Node();
        n9.setData("nine");
        n6.setNext(n9);
        Node current5 = n8;
        while(current5 != null){
            System.out.print(current5.getData() + " ");
            current5 = current5.getNext();
        }

        System.out.println("Functions");
        Node head = n8;
        printAllNodes(head);
        size(head);

        //insertIntoIndex(head, n7, index);
        //public static void addAtFront(Node head, Node n);
        //public static void addAtEnd(Node head, Node n);
    }

    public static void printAllNodes(Node head){
        while(head != null){
            System.out.print(head.getData() + " ");
            head = head.getNext();
        }
    }


    public static void size(Node head){
        int counter = 0;
        Node currentCounter = head;
        while(currentCounter != null){
            counter++;
        }
    }

//    public static void insertIntoIndex(Node head, Node n7, int index){
//
//    }
//    public static void addAtFront(Node head, Node n);
//    public static void addAtEnd(Node head, Node n);
}
