package kz.aitu.linkedlist;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        LinkedList list = new LinkedList();

//        Block b1 = new Block(5);
//        Block b2 = new Block(4);
//        Block b3 = new Block(1);
//        Block b4 = new Block(2);
//        list.addBlock(b1);
//        list.addBlock(b2);
//        list.addBlock(b3);
//        list.addBlock(b4);

        Scanner scanner = new Scanner(System.in);
        for(int i = 0; i<10; i++){
            int a = scanner.nextInt();
            list.addBlock(new Block(a));
        }

        Block b = list.getHead();
        while(b != null){
            System.out.println(b.getData() + " ");
            b = b.getNextBlock();
        }
        System.out.println();
    }
}
