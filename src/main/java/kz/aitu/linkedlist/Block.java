package kz.aitu.linkedlist;

public class Block {
    private int data;
    private Block nextBlock;

    public int getData() {
        return data;
    }

    public Block getNextBlock() {
        return nextBlock;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setNextBlock(Block nextBlock) {
        this.nextBlock = nextBlock;
    }

    public Block(int data) {
        this.data = data;
    }

    //    public int getData() {
//        return data;
//    }
//
//    public Block getNextBlock() {
//        return nextBlock;
//    }
//
//    public void setData(int data) {
//        this.data = data;
//    }
//
//    public void setNextBlock(Block nextBlock) {
//        this.nextBlock = nextBlock;
//    }

    //    public Block(int value){
//        this.value = value;
//    }
}
