package kz.aitu.linkedlist;

import lombok.Data;

@Data

public class LinkedList {
    private Block head;
    private Block tail;

    public void addBlock(Block block){
        if (head == null){
            head = block;
            tail = block;
        } else {
            tail.setNextBlock(block);
            tail = block;
        }
    }

    public Block getHead() {
        return head;
    }

    public Block getTail() {
        return tail;
    }

    public void setHead(Block head) {
        this.head = head;
    }

    public void setTail(Block tail) {
        this.tail = tail;
    }
}
