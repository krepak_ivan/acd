package kz.aitu;

import java.util.Scanner;

public class MazeTask {
    public static void main(String[] args) {
        int n;
        int m;
        Scanner input = new Scanner(System.in);
        n = input.nextInt();
        m = input.nextInt();
        int[][] mazeArray = new int[n][m];

        for(int q = 0; q<n; q++){
            for(int w = 0; w<m; w++){
                mazeArray[q][w] = input.nextInt();
            }
        }
        System.out.println(countPaths(mazeArray, n, m));
    }

    public static int countPaths(int[][] mazeArray, int n, int m){
        int answer;
        if(mazeArray[0][0] == 1){
            answer = 0;
        }
        for (int i = 0; i < n; i++){
            if(mazeArray[i][0] == 0){
                mazeArray[i][0] = 1;
            }else{
                break;
            }
        }
        for (int i = 1; i < m; i++){
            if (mazeArray[0][i] == 0){
                mazeArray[0][i] = 1;
            }else{
                break;
            }
        }
        for (int i = 1; i < n; i++){
            for (int j = 1; j < m; j++){
                if(mazeArray[i][j] == -1){
                    continue;
                }
                if (mazeArray[i - 1][j] > 0){
                    mazeArray[i][j] = (mazeArray[i][j] + mazeArray[i - 1][j]);
                }
                if (mazeArray[i][j - 1] > 0){
                    mazeArray[i][j] = (mazeArray[i][j] + mazeArray[i][j - 1]);
                }
            }
        }
        answer = (mazeArray[n - 1][m - 1] > 0)? mazeArray[n - 1][m - 1] : 0;
        return answer;
    }
}