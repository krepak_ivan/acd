package kz.aitu.queue;

//import lombok.Data;
//@Data

public class Queue {
    Node head;
    Node tail;

    public Node getHead() {
        return head;
    }

//    public void setHead(Node head) {
//        this.head = head;
//    }

    public void add(int data){
        Node newNode = new Node(data);
        if(tail == null){
            head = newNode;
            tail = newNode;
        }else{
            tail.next = newNode;
            newNode.next = null;
            tail = newNode;
        }
    }

    public int size(){
        int counter = 0;
        Node tempNode = head;
        while(tempNode != null){
            tempNode = tempNode.next;
            counter++;
        }
        return counter;
    }

    public void remove(){
        head = head.next;
    }

    public int peek() {
        if (head == null) {
            return -1;
        } else{
            return head.data;
        }
    }

    public Node poll(){
        Node previousHead = head;
        if(head == null) {
            return null;
        }else{
            head = head.next;
            return previousHead;
        }
        }
    }