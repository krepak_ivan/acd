package kz.aitu.queue;

public class Main {
    public static void main(String[] args) {
        Queue q = new Queue();
        q.add(1);
        q.add(2);
        q.add(3);
        q.add(4);
        q.add(5);
        q.add(6);
        q.add(7);
        q.add(8);
        q.add(9);
        q.add(10);

        q.poll();
        q.remove();

        System.out.println("Peak element is: " + q.peek());
        System.out.println("Size is: " + q.size());

        System.out.println("Printing queue: ");
        Node current = q.head;
        while(current != null){
            System.out.print(current.data + " ");
            current = current.next;
        }
    }
}
