package kz.aitu.week9.cs1902quiz.task_four;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String s;
        String t;
        Boolean anagram = false;
        Scanner input = new Scanner(System.in);

        System.out.println("Enter s");
        s = input.nextLine();
        System.out.println("Enter t");
        t = input.nextLine();

        if(s.length() != t.length()) anagram = false;
        if(s == t) anagram = false;

        char sArray[] = s.toCharArray();
        char tArray[] = t.toCharArray();

        char reverseTArray[] = new char[t.length()];
        int q = 0;
        for(int i = t.length() - 1; i >= 0; i--){
            reverseTArray[q] = tArray[i];
            q++;
        }

        for(int w = 0; w < s.length(); w++){
            if(sArray[w] == reverseTArray[w]){
                anagram = true;
            }else anagram = false;
        }

        System.out.println(anagram);
    }
}
