package kz.aitu.week9.sum_of_elements;

public class Main {
    public static int n1 = 0, n2 = 0, n3 = 0, n4 = 0, n5 = 0, n6 = 0, n7 = 0, n8 = 0, n9 = 0, n10 = 0;
    public static int[] array = {1, 3, 2, 1, 10, 7, 8, 10, 1, 2};
    public static int number = 10;

    public static void main(String[] args) {
        int dif;

        for(int i = 0; i < array.length; i++){
            dif = number - array[i];
            for(int j = 1; j < array.length; j++){
                if(dif == array[j]){
                    n1 = array[i];
                    n2 = array[j];
                    System.out.println(i + ": " + n1);
                    System.out.println(j - 1 + ": " + n2);
                }
                break;
            }
        }

        if(n1 == 0 && n2 == 0){ //3 numbers
            threeNumbers();
        }

        if(n1 == 0 && n2 == 0 && n3 == 0){ //4 numbers
            fourNumbers();
        }

        if(n1 == 0 && n2 == 0 && n3 == 0 && n4 == 0){ //5 numbers
            fiveNumbers();
        }

        if(n1 == 0 && n2 == 0 && n3 == 0 && n4 == 0 && n5 == 0){ //6 numbers
            sixNumbers();
        }

        if(n1 == 0 && n2 == 0 && n3 == 0 && n4 == 0 && n5 == 0 && n6 == 0){ //7 numbers
            sevenNumbers();
        }

        if(n1 == 0 && n2 == 0 && n3 == 0 && n4 == 0 && n5 == 0 && n6 == 0 && n7 == 0){ //8 numbers
            eightNumbers();
        }

        if(n1 == 0 && n2 == 0 && n3 == 0 && n4 == 0 && n5 == 0 && n6 == 0 && n7 == 0 && n8 == 0){ //9 numbers
            nineNumbers();
        }

        if(n1 == 0 && n2 == 0 && n3 == 0 && n4 == 0 && n5 == 0 && n6 == 0 && n7 == 0 && n8 == 0 && n9 == 0){ //10 numbers
            tenNumbers();
        }
    }

    public static void threeNumbers(){
        int i = 0, j = 1, k = 2;
        n1 = array[i];
        n2 = array[j];
        n3 = array[k];

        while(n1 != 0 && n2 != 0 && n3 != 0){
            if((n1 + n2 + n3) == number){
                System.out.println(i + ": " + n1);
                System.out.println(j + ": " + n2);
                System.out.println(k + ": " + n3);
            }
            i++;
            j++;
            k++;
        }
        if(i == number && j == number && k == number) fourNumbers();
    }

    public static void fourNumbers(){
        int i = 0, j = 1, k = 2, q = 3;
        n1 = array[i];
        n2 = array[j];
        n3 = array[k];
        n4 = array[q];

        if((n1 + n2 + n3 + n4) == number){
            System.out.println(i + ": " + n1);
            System.out.println(j + ": " + n2);
            System.out.println(k + ": " + n3);
            System.out.println(q + ": " + n4);
            i++; j++; k++; q++;
            if(i == number && j == number && k == number && q == number) fiveNumbers();
        }
    }

    public static void fiveNumbers(){
        if(n1 == 0 && n2 == 0 && n3 == 0 && n4 == 0){ //5 numbers
            int i = 0, j = 1, k = 2, q = 3, w = 4;
            n1 = array[i];
            n2 = array[j];
            n3 = array[k];
            n4 = array[q];
            n5 = array[w];

            if((n1 + n2 + n3 + n4 + n5) == number){
                System.out.println(i + ": " + n1);
                System.out.println(j + ": " + n2);
                System.out.println(k + ": " + n3);
                System.out.println(q + ": " + n4);
                System.out.println(w + ": " + n5);
                i++; j++; k++; q++; w++;
            }
            if(i == number && j == number && k == number && q == number && w == number) sixNumbers();
        }
    }

    public static void sixNumbers(){
        int i = 0, j = 1, k = 2, q = 3, w = 4, e = 5;
        n1 = array[i];
        n2 = array[j];
        n3 = array[k];
        n4 = array[q];
        n5 = array[w];
        n6 = array[e];

        if((n1 + n2 + n3 + n4 + n5 + n6) == number){
            System.out.println(i + ": " + n1);
            System.out.println(j + ": " + n2);
            System.out.println(k + ": " + n3);
            System.out.println(q + ": " + n4);
            System.out.println(w + ": " + n5);
            System.out.println(e + ": " + n6);
            i++; j++; k++; q++; w++; e++;
            if(i == number && j == number && k == number && q == number && w == number && e == number) sevenNumbers();
        }
    }

    public static void sevenNumbers(){
        int i = 0, j = 1, k = 2, q = 3, w = 4, e = 5, r = 6;
        n1 = array[i];
        n2 = array[j];
        n3 = array[k];
        n4 = array[q];
        n5 = array[w];
        n6 = array[e];
        n7 = array[r];

        if((n1 + n2 + n3 + n4 + n5 + n6 + n7) == number){
            System.out.println(i + ": " + n1);
            System.out.println(j + ": " + n2);
            System.out.println(k + ": " + n3);
            System.out.println(q + ": " + n4);
            System.out.println(w + ": " + n5);
            System.out.println(e + ": " + n6);
            System.out.println(r + ": " + n7);
            i++; j++; k++; q++; w++; e++; r++;
            if(i == number && j == number && k == number && q == number && w == number && e == number && r == number)
                eightNumbers();
        }
    }

    public static void eightNumbers(){
        int i = 0, j = 1, k = 2, q = 3, w = 4, e = 5, r = 6, t = 7;
        n1 = array[i];
        n2 = array[j];
        n3 = array[k];
        n4 = array[q];
        n5 = array[w];
        n6 = array[e];
        n7 = array[r];
        n8 = array[t];

        if((n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8) == number){
            System.out.println(i + ": " + n1);
            System.out.println(j + ": " + n2);
            System.out.println(k + ": " + n3);
            System.out.println(q + ": " + n4);
            System.out.println(w + ": " + n5);
            System.out.println(e + ": " + n6);
            System.out.println(r + ": " + n7);
            System.out.println(t + ": " + n8);
            i++; j++; k++; q++; w++; e++; r++; t++;
            if(i == number && j == number && k == number && q == number && w == number && e == number && r == number
            && t == number) nineNumbers();
        }
    }

    public static void nineNumbers(){
        int i = 0, j = 1, k = 2, q = 3, w = 4, e = 5, r = 6, t = 7, y = 8;
        n1 = array[i];
        n2 = array[j];
        n3 = array[k];
        n4 = array[q];
        n5 = array[w];
        n6 = array[e];
        n7 = array[r];
        n8 = array[t];
        n9 = array[y];

        if((n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9) == number){
            System.out.println(i + ": " + n1);
            System.out.println(j + ": " + n2);
            System.out.println(k + ": " + n3);
            System.out.println(q + ": " + n4);
            System.out.println(w + ": " + n5);
            System.out.println(e + ": " + n6);
            System.out.println(r + ": " + n7);
            System.out.println(t + ": " + n8);
            System.out.println(y + ": " + n9);
            i++; j++; k++; q++; w++; e++; r++; t++; y++;
            if(i == number && j == number && k == number && q == number && w == number && e == number && r == number
                    && t == number && y == number) tenNumbers();
        }
    }

    public static void tenNumbers(){
        int i = 0, j = 1, k = 2, q = 3, w = 4, e = 5, r = 6, t = 7, y = 8, u = 9;
        n1 = array[i];
        n2 = array[j];
        n3 = array[k];
        n4 = array[q];
        n5 = array[w];
        n6 = array[e];
        n7 = array[r];
        n8 = array[t];
        n9 = array[y];
        n10 = array[u];

        if((n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + n10) == number){
            System.out.println(i + ": " + n1);
            System.out.println(j + ": " + n2);
            System.out.println(k + ": " + n3);
            System.out.println(q + ": " + n4);
            System.out.println(w + ": " + n5);
            System.out.println(e + ": " + n6);
            System.out.println(r + ": " + n7);
            System.out.println(t + ": " + n8);
            System.out.println(y + ": " + n9);
            System.out.println(u + ": " + n10);
            i++; j++; k++; q++; w++; e++; r++; t++; y++; u++;
            if(i == number && j == number && k == number && q == number && w == number && e == number && r == number
                    && t == number && y == number && u == number) return;
        }
    }
}