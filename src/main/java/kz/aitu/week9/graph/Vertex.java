package kz.aitu.week9.graph;

public class Vertex<Key, Value> {
    private Key key;
    private Value value;
    private Vertex next;
    private Vertex nextForLL;
    private LinkedList<Key> edgesList;

    public Vertex(Key key, Value value) {
        this.key = key;
        this.value = value;
        edgesList = new LinkedList();
    }

    public void addEdge(Vertex vertex) {
        if(!edgesList.ifContains(vertex)) edgesList.add(vertex);
    }

    public void relatesWith(){
        if(!edgesList.isEmpty()) edgesList.connectedWith();
        else System.out.println("No connections of " + key);
    }

    public boolean isConnected(Key key2){
        if(edgesList.isEmpty()) return false;
        return edgesList.isConnected(key2);
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public Key getKey() {
        return key;
    }

    public Value getValue() {
        return value;
    }

    public void setNext(Vertex next) {
        this.next = next;
    }

    public Vertex getNext() {
        return next;
    }

    public Vertex getNextForLL() {
        return nextForLL;
    }

    public void setNextForLL(Vertex nextForLL) {
        this.nextForLL = nextForLL;
    }
}

