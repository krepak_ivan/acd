package kz.aitu.week9.graph;

public class Node {
    private int data;
    private Node left;
    private Node right;

    public Node(int data){
        this.data = data;
        left = null;
        right = null;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}