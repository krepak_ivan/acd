package kz.aitu.week9.graph;

public class LinkedList<Key> {
    private Vertex first;
    private Vertex last;

    public LinkedList(){
        first = null;
        last = null;
    }

    public void add(Vertex vertex){
        if(first == null){
            first = vertex;
            last = vertex;
        }else{
            last.setNextForLL(vertex);
            last = vertex;
        }
    }

    public void connectedWith(){
        int counter = 0;
        int i = 0;
        Vertex current = first;
        Vertex currentForPrint = first;
        String toOutput;

        while(current != null){
            counter++;
            current = current.getNextForLL();
        }

        while(currentForPrint != null){
            toOutput = currentForPrint.getKey() + " " + currentForPrint.getValue();
            currentForPrint = currentForPrint.getNextForLL();

            while(i < 1){
                System.out.println("There are connection(s) with " + counter + " vertex(es)");
                i++;
            }

            System.out.println(toOutput);
        }
    }

    public boolean ifContains(Vertex vertex){
        Vertex current = first;

        if(first != null){
            while(current != null){
                if(current == vertex) return true;
                current = current.getNextForLL();
            }

            return false;
        }

        return false;
    }

    public boolean isConnected(Key key2){
        Vertex current = first;

        while(current != null){
            if(current == key2) return true;
            current = current.getNextForLL();
        }

        return false;
    }

    public boolean isEmpty(){
        return first == null;
    }
}