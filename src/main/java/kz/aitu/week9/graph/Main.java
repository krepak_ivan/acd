package kz.aitu.week9.graph;

public class Main {
    public static void main(String[] args) {
        Graph graph = new Graph<Integer, String>();

        graph.addVertex(1, "User1");
        graph.addVertex(2, "User2");
        graph.addVertex(3, "User3");
        graph.addVertex(4, "User4");
        graph.addVertex(5, "User5");
        graph.addVertex(6, "User6");
        graph.addVertex(7, "User7");
        graph.addVertex(8, "User8");
        graph.addVertex(9, "User9");
        graph.addVertex(10, "User10");

        graph.addEdge(1, 3, true);
        graph.addEdge(1, 4, false);
        graph.addEdge(4, 3, false);
        graph.addEdge(4, 2, true);
        graph.addEdge(5, 4, false);
        graph.addEdge(5, 10, true);
        graph.addEdge(2, 6, true);
        graph.addEdge(6, 7, false);
        graph.addEdge(8, 9, false);
        graph.addEdge(9, 7, true);

        graph.relatesWith(1);
        graph.relatesWith(2);
        graph.relatesWith(4);
        graph.relatesWith(10);

        System.out.println(graph.isConnected(2,7));
        System.out.println(graph.isConnected(2,6));

        graph.printAll();
        System.out.println();

        graph.countVertex();
    }
}