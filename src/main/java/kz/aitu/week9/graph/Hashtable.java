package kz.aitu.week9.graph;

public class Hashtable<Key, Value> {
    private Vertex<Key, Value> vertexTable[];
    private int size = 11;

    public Hashtable(){
        vertexTable = new Vertex[11];
    }

    public void place(Key key, Vertex vertex){
        if(vertexTable[key.hashCode() % size] == null) vertexTable[key.hashCode() % size] = vertex;
        else{
            Vertex current = vertexTable[key.hashCode() % size];

            while(current != null){
                if(current.getKey() == vertex.getKey()){
                    current.setValue(vertex.getValue());
                    break;
                }

                current = current.getNext();
            }

            if(current == null){
                vertex.setNext(vertexTable[key.hashCode() % size]);
                vertexTable[key.hashCode() % size] = vertex;
            }
        }
    }

    public Vertex get(Key key){
        if(vertexTable[key.hashCode() % size] != null){
            Vertex current = vertexTable[key.hashCode() % size];

            while(current != null){
                if (current.getKey() == key) return current;
                current = current.getNext();
            }
        }

        return null;
    }

    public boolean ifKeyExists(Key key){
        if(vertexTable[key.hashCode() % size] != null){
            Vertex current = vertexTable[key.hashCode() % size];

            while(current != null){
                if(current.getKey() == key) return true;
                current = current.getNext();
            }
        }

        return false;
    }

    public void printAll(){
        for(int i = 0;i < size; i++){
            Vertex current = vertexTable[i];

            while(current != null){
                System.out.print(current.getValue() + " ");
                current = current.getNext();
            }
        }
    }

    public int countVertex(){
        int count = 0;

        for(int i = 0;i < size; i++){
            Vertex current = vertexTable[i];

            while(current != null){
                count++;
                current = current.getNext();
            }
        }

        return count;
    }
}