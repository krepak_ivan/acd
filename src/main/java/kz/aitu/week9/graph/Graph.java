package kz.aitu.week9.graph;

public class Graph<Key, Value> {
    private boolean biDirectional;
    private Hashtable<Key,Value> vertexTable = new Hashtable<Key, Value>();

    public void addVertex(Key key, Value value){
        Vertex vertex = new Vertex<Key, Value>(key, value);

        if(vertexTable.ifKeyExists(key)) return;
        else vertexTable.place(key, vertex);
    }

    public void addEdge(Key key1, Key key2, boolean biDirectional) {
        Vertex vertexA = vertexTable.get(key1);
        Vertex vertexB = vertexTable.get(key2);

        vertexA.addEdge(vertexB);
        if(biDirectional) vertexB.addEdge(vertexA);
    }

    public void relatesWith(Key key){
        Vertex vertex = vertexTable.get(key);
        vertex.relatesWith();
    }

    public boolean isConnected(Key key1, Key key2){
        Vertex vertexA = vertexTable.get(key1);
        Vertex vertexB = vertexTable.get(key2);

        return vertexA.isConnected(vertexB);
    }

    public void printAll(){
        vertexTable.printAll();
    }

    public void countVertex(){
        System.out.println(vertexTable.countVertex());
    }
}