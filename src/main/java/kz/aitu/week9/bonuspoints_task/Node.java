package kz.aitu.week9.bonuspoints_task;

public class Node {
    String key;
    String data;

    private int value;
    private Node left;
    private Node right;

    int keyRight;
    int keyLeft;
    int rightRight;
    int rightLeft;
    int leftRight;
    int leftLeft;
    int one = 0;
    int two = 0;

    public Node(Integer value, int key){
        this.keyRight = 0;
        this.keyLeft = 0;
        this.value = value;
        this.left = null;
        this.right = null;
    }

    public Node(String data){
        this.data = data;
    }

    public String data(){
        return data;
    }

    public int  getOne(){
        return this.one;
    }

    public int setOne(int oneOne){
        return  this.one = oneOne + 1;
    }

    public int  getTwo(){
        return this.two;
    }

    public int setTwo(int twoTwo){
        return this.two = twoTwo + 1;
    }

    public String getKey(){
        return this.key;
    }

    public int getValue(){
        return value;
    }

    public void setValue(int value){
        this.value = value;
    }

    public Node getLeft(){
        return left;
    }

    public void setLeft(Node left){
        this.left = left;
    }

    public Node getRight(){
        return right;
    }

    public void setRight(Node right){
        this.right = right;
    }

    public int  getRightRight(){
        return this.rightRight;
    }

    public void setRightRight(int rightRight){
        this.rightRight = rightRight;
    }

    public int  getRightLeft(){
        return this.rightLeft;
    }

    public void setRightLeft(int rightLeft){
        this.rightLeft = rightLeft;
    }

    public int  getLeftRight(){
        return this.leftRight;
    }

    public void setLeftRight(int leftRight){
        this.leftRight = leftRight;
    }

    public int  getLeftLeft(){
        return this.leftLeft;
    }

    public void setLeftLeft(int leftLeft){
        this.leftLeft = leftLeft;
    }

    public int  getKeyRight(){
        return this.keyRight;
    }

    public void setKeyRight(int keyRight){
        this.keyRight = keyRight;
    }

    public int  getKeyLeft(){
        return this.keyLeft;
    }

    public void setKeyLeft(int keyLeft){
        this.keyLeft= keyLeft;
    }

    public String toString(){
        return this.data;
    }
}