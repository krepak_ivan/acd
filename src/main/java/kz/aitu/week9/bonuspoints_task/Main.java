package kz.aitu.week9.bonuspoints_task;

public class Main {
    public static void main(String[] args) {
        BonusPoints bonusPoints = new BonusPoints();

        bonusPoints.addRoot("Student1");
        bonusPoints.addChild("Student2", "Student1");
        bonusPoints.addChild("Student3", "Student2");
        bonusPoints.addChild("Student4", "Student3");
        bonusPoints.addChild("Student5", "Student3");
        bonusPoints.addChild("Student6", "Student5");

        bonusPoints.addChild("Student7", "Student5");
        bonusPoints.addChild("Student8", "Student2");

        System.out.println(bonusPoints.getPoints("Student1")); //returns 93

        bonusPoints.addChild("Student9", "Student3");
        bonusPoints.addChild("Student6", "Student5");
        bonusPoints.addRoot("Student1");
        bonusPoints.addChild("Student11", "Student10");

        System.out.println("Student1 : " + bonusPoints.getPoints("Student1"));
        System.out.println("Student2 : " + bonusPoints.getPoints("Student2"));
        System.out.println("Student3 : " + bonusPoints.getPoints("Student3"));
        System.out.println("Student4 : " + bonusPoints.getPoints("Student4"));
        System.out.println("Student5 : " + bonusPoints.getPoints("Student5"));
        System.out.println("Student6 : " + bonusPoints.getPoints("Student6"));
        System.out.println("Student7 : " + bonusPoints.getPoints("Student7"));
        System.out.println("Student8 : " + bonusPoints.getPoints("Student8"));

        System.out.println("Student9 : " + bonusPoints.getPoints("Student9"));
    }
}