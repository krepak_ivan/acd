package kz.aitu.week9.bonuspoints_task;

public class BonusPoints {
    Node BPNode;
    Node parentParentNode = new Node(null);
    Node rootNode = new Node(null);
    Node[] array = new Node[50];
    Node flag;
    Node flagTwo;
    Node childOne;
    Node childTwo;
    Node rootLeft;
    Node rootRight;
    Node addedNode;
    Node itsChild;
    Node pointsNode;
    Node lNode;
    Node rNode;

    int q = 0;
    int w = 1;
    int counter = 0;
    int counterTwo = 0;
    int intChildOne;
    int intChildTwo;
    int counterThree = 0;
    int arrayElement = 0;

    boolean temp = false;
    boolean tempTwo = false;
    boolean isFound = false;
    boolean ifPoints;

    public Node parentParentNode(String parent, int counterTwo, int a, int s){
        if(counterTwo == 0){
            addedNode = rootNode;
            counterTwo++;
        }

        if(counterTwo != 0){
            if(addedNode != null){
                if(addedNode.getRight() != null){
                    if(addedNode.getRight().getRight() != null){
                        if(addedNode.getRight().getRight().getRight() != null){
                            if (a >= 3) addedNode = adding(addedNode);
                        }
                    }

                    if(addedNode.getRight().getRight() != null){
                        if(addedNode.getRight().getLeft() != null){
                            if(addedNode.getRight().getLeft().getRight() != null){
                                if(a >= 3) addedNode = adding(addedNode);
                            }
                        }
                    }
                }

                if(addedNode.getLeft() != null){
                    if(addedNode.getLeft().getRight() != null){
                        if(addedNode.getLeft().getRight().getRight() != null){
                            if (a >= 3) addedNode = adding(addedNode);
                        }
                    }

                    if(addedNode.getLeft().getRight() != null){
                        if(addedNode.getLeft().getLeft() != null){
                            if(addedNode.getLeft().getLeft().getRight() != null){
                                if(a >= 3) addedNode = adding(addedNode);
                            }
                        }
                    }
                }
            }
        }

        if(addedNode.getRight() != null){
            if(addedNode.getRight() != null){
                if(addedNode.getRightRight() != 1){
                    if(addedNode.getRight().getRight() != null){
                        int y = addedNode.getValue() + 2;
                        addedNode.setValue(y);
                        addedNode.setRightRight(1);
                    }
                }

                if(addedNode.getRightLeft() != 1){
                    if(addedNode.getRight().getLeft() != null){
                        int y = addedNode.getValue() + 1;
                        addedNode.setValue(y);
                        addedNode.setRightLeft(1);
                    }
                }
            }

            if(addedNode.getLeft() != null){
                if(addedNode.getLeftRight() != 1){
                    if(addedNode.getLeft().getRight() != null){
                        int y = addedNode.getValue() + 1;
                        addedNode.setValue(y);
                        addedNode.setLeftRight(1);
                    }
                }

                if(addedNode.getLeftLeft() != 1){
                    if(addedNode.getLeft().getLeft() != null){
                        int y = addedNode.getValue() + 1;
                        addedNode.setValue(y);
                        addedNode.setLeftLeft(1);
                    }
                }
            }

            if(addedNode.getLeft() != null){
                addedNode = addedNode.getLeft();
                parentParentNode(parent, counterTwo, a, s);
            }

            if(addedNode.getRight() != null){
                addedNode = addedNode.getRight();
                parentParentNode(parent, counterTwo, a, s);
            }
        }

        if(addedNode.getLeft() != null){
            if(addedNode.getRight() != null){
                if(addedNode.getRightRight() != 1){
                    if(addedNode.getRight().getRight() != null){
                        int y = addedNode.getValue() + 2;
                        addedNode.setValue(y);
                        addedNode.setRightRight(1);
                    }
                }

                if(addedNode.getRightLeft() != 1){
                    if(addedNode.getRight().getLeft() != null){
                        int y = addedNode.getValue() + 1;
                        addedNode.setValue(y);
                        addedNode.setRightLeft(1);
                    }
                }
            }

            if(addedNode.getLeft() != null){
                if(addedNode.getLeftRight() != 1){
                    if(addedNode.getLeft().getRight() != null){
                        int y = addedNode.getValue() + 1;
                        addedNode.setValue(y);
                        addedNode.setLeftRight(1);
                    }
                }

                if(addedNode.getLeftLeft() != 1){
                    if(addedNode.getLeft().getLeft() != null){
                        int y = addedNode.getValue() + 1;
                        addedNode.setValue(y);
                        addedNode.setLeftLeft(1);
                    }
                }
            }

            if(addedNode.getLeft() != null){
                addedNode = addedNode.getLeft();
                parentParentNode(parent, counterTwo, a, s);
            }

            if(addedNode.getRight() != null){
                addedNode = addedNode.getRight();
                parentParentNode(parent, counterTwo, a, s);
            }
        }

        return addedNode;
    }

    public void addRoot(String name){
        rootNode = new Node(name);
        rootNode.setValue(80);

        if(array[0] == null){
            array[0] = rootNode;
            q++;
            w++;
        }

        if(checkArray(name) == false){
            rootNode = new Node(name);
            rootNode.setValue(80);

            while(q < w){
                array[q] = rootNode;
                q++;
            }

            w++;
        }else return;
    }

    public void addChild(String child, String parent){
        Node Child = new Node(child);
        Child.setValue(80);
        Node Parent;

        if(parent == rootNode.data()){
            for(int i = 0; i < 50; i++){
                if(array[i] != null){
                    Node rootTempNode=array[i];

                    if(rootNode.data == rootTempNode.data) rootNode = rootTempNode;
                }else break;
            }

            Parent = rootNode;

            if(areTwoChild(child) == true) return;
        }else{
            if(areTwoChild(child) == true) return;
            Parent = find(parent, 0);
        }

        if(Parent==null) return;
        if(Parent.getRight() == null){
            Parent.setRight(Child);
            intChildOne = rootNode.getOne();
            intChildOne= intChildOne +  Parent.setOne(intChildOne);
        }else if(Parent.getLeft() == null){
            Parent.setLeft(Child);
            intChildTwo = rootNode.getTwo();
            intChildTwo= intChildTwo +  Parent.setTwo(intChildTwo);
        }else{
            System.out.println("You can't add third student");
            return;
        }

        if(Parent.getKeyRight() == 0){
            if(Parent.getRight() != null){
                Parent.setKeyRight(1);
                int y = Parent.getValue() + 5;
                Parent.setValue(y);
            }
        }

        if(Parent.getKeyLeft() == 0){
            if(Parent.getLeft() != null){
                Parent.setKeyLeft(1);
                int y = Parent.getValue() + 5;
                Parent.setValue(y);
            }
        }

        parentParentNode(parent, 0, intChildOne, intChildTwo);
        rootLeft = rootNode.getLeft();
        rootRight = rootNode.getRight();
    }

    public Node adding(Node addedNode){
        int y = addedNode.getValue() + 1;
        addedNode.setValue(y);

        return addedNode;
    }

    public Boolean areTwoChild(String name){
        Node Root = new Node(name);
        itsChild = array[0];

        if(findChild(name, 0, itsChild.getLeft(), itsChild.getRight()) == false){
            for(int arrayElement = 1; arrayElement < 20;){
                if(array[arrayElement] != null){
                    Node tempTemp = array[arrayElement];
                    itsChild = tempTemp;

                    if(arrayElement > 0 && findChild(name, 0, itsChild.getLeft(), itsChild.getRight()) == true) return true;
                    if(tempTemp.data != Root.data()) arrayElement++;
                }else return false;
            }
        }

        return true;
    }

    public boolean checkArray(String name){
        for(int arrayElement = 0; arrayElement < 20;){
            Node flagThree = array[arrayElement];

            if(flagThree != null){
                if(flagThree.data != name){
                    arrayElement++;
                    temp = false;
                }else{
                    temp = true;
                    break;
                }
            }else break;
        }

        return temp;
    }

    public boolean findChild(String child, int counterFour, Node childTwo, Node childOne){
        if(rootNode.getLeft() != null || rootNode.getLeft() != null){
            if(counterFour == 0){
                tempTwo = false;
                childOne = rootNode.getLeft();
                childTwo = rootNode.getRight();
                counterFour++;
            }
        }else if(counterFour == 0){
            tempTwo = false;
            counterFour++;
        }

        if(childOne != null){
            if(childOne.data == child){
                tempTwo = true;
                ifPoints = true;

                return true;
            }else{
                if(childOne.getLeft() != null) findChild(child, counterFour, childTwo, childOne.getLeft());
                if(childOne.getRight() != null) findChild(child, counterFour, childTwo, childOne.getRight());
                if(tempTwo == true) return tempTwo;
            }
        }

        if(childTwo != null){
            if(childTwo.data == child){
                tempTwo = true;
                ifPoints=true;

                return true;
            }else{
                if(childTwo.getLeft() != null) findChild(child, counterFour, childTwo.getLeft(), childOne);
                if(childTwo.getRight() != null) findChild(child, counterFour, childTwo.getRight(), childOne);
                if(tempTwo == true) return tempTwo;
            }
        }

        return false;
    }

    public Node find(String parent, int counter){
        if(counter == 0){
            rootLeft = rootNode;
            rootRight = rootNode;
            rootLeft = rootNode.getLeft();
            rootRight = rootNode.getRight();
            counter++;
            isFound=false;
        }

        if(rootLeft != null){
            if(rootLeft.data == parent){
                isFound = true;
                return rootLeft;
            }else{
                if(rootLeft.getLeft() != null){
                    rootLeft = rootLeft.getLeft();
                    find(parent, counter);
                }

                if(isFound==true) return rootLeft;
                if(rootLeft.getRight() != null){
                    rootLeft = rootLeft.getRight();
                    find(parent, counter);
                }

                if(isFound==true) return rootLeft;
            }
        }

        if(rootRight != null){
            if(rootRight.data == parent){
                isFound = true;
                return rootRight;
            }else{
                if(rootRight.getLeft() != null){
                    rootRight = rootRight.getLeft();
                    find(parent, counter);
                }

                if(isFound == true) return rootRight;
                if(rootRight.getRight() != null){
                    rootRight = rootRight.getRight();
                    find(parent, counter);
                }

                if(isFound == true) return rootRight;
            }
        }

        return null;
    }

    public int getPoints(String name){
        return getPoints(name, lNode, rNode);
    }

    public int getPoints(String name, Node lNode, Node rNode){
        Node Root = new Node(name);

        if(checkArray(name) == true){
            flagTwo = array[arrayElement];
            rootNode = flagTwo;

            while(flagTwo.data != Root.data()){
                for(int arrayElement = 0; arrayElement < 20;){
                    flagTwo = array[arrayElement];

                    if(flagTwo.data != Root.data()) arrayElement++;
                    else{
                        rootNode = flagTwo;
                        break;
                    }
                }

                if(flagTwo.data == Root.data()){
                    rootNode = flagTwo;
                    break;
                }
            }

            if(counterThree == 0){
                lNode = rootNode.getLeft();
                rNode = rootNode.getRight();
                counterThree++;
            }

            if(rootNode.data == name) return rootNode.getValue();
            if(lNode != null){
                if(lNode.data == name){
                    pointsNode = lNode;
                    return lNode.getValue();
                }else{
                    if(lNode.getLeft() != null) getPoints(name, lNode.getLeft(), rNode);
                    if(lNode.getRight() != null) getPoints(name, lNode.getRight(), rNode);
                    if(pointsNode != null) return pointsNode.getValue();
                }
            }

            if(rNode != null){
                if(rNode.data == name){
                    pointsNode = rNode;
                    return rNode.getValue();
                }else{
                    if(rNode.getLeft() != null) getPoints(name, lNode, rNode.getLeft());
                    if(rNode.getRight() != null) getPoints(name, lNode, rNode.getRight());
                    if(pointsNode != null) return pointsNode.getValue();
                }
            }
        }else{
            flagTwo = array[arrayElement];
            rootNode = flagTwo;

            if(findChild(name, 0, rootNode.getLeft(), rootNode.getRight()) == false){
                for(int arrayElement = 1; arrayElement < 20;){
                    if(array[arrayElement] != null){
                        flag = array[arrayElement];
                        rootNode = flag;

                        if(arrayElement > 0 && findChild(name, 0, rootNode.getLeft(), rootNode.getRight()) == true){
                            ifPoints = true;
                            break;
                        }

                        if(flag.data != Root.data()) arrayElement++;
                    }else break;
                }
            }

            if(ifPoints == true) return printing(name, lNode, rNode, 0);
        }

        return 0;
    }

    public int printing(String name, Node lNode, Node rNode, int counterFive){
        ifPoints = false;

        if(counterFive == 0){
            lNode = rootNode.getLeft();
            rNode = rootNode.getRight();
            counterFive++;
        }

        if(rootNode.data == name) return rootNode.getValue();
        if(lNode != null){
            if(lNode.data == name){
                pointsNode = lNode;
                return lNode.getValue();
            }else{
                if(lNode.getLeft() != null) printing(name, lNode.getLeft(), rNode, counterFive);
                if(lNode.getRight() != null) printing(name, lNode.getRight(), rNode, counterFive);
            }
        }

        if(rNode != null){
            if(rNode.data == name){
                pointsNode = rNode;
                return rNode.getValue();
            }else{
                if(rNode.getLeft() != null) printing(name, lNode, rNode.getLeft(), counterFive);
                if(rNode.getRight() != null) printing(name, lNode, rNode.getRight(), counterFive);
            }

            if(pointsNode != null) return pointsNode.getValue();
        }

        return 0;
    }
}