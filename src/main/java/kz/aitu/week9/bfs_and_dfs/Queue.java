package kz.aitu.week9.bfs_and_dfs;

public class Queue {
    private QueueNode head;
    private QueueNode tail;

    public void push(int key){
        QueueNode newQueueNode = new QueueNode(key);

        if(head == null){
            head = newQueueNode;
            tail = newQueueNode;
        }else{
            tail.setNext(newQueueNode);
            tail = newQueueNode;
        }
    }

    public int pop(){
        QueueNode current = head;
        head = head.getNext();

        return current.getKey();
    }

    public boolean isEmpty(){
        return head == null;
    }
}
