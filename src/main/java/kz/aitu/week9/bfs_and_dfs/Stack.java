package kz.aitu.week9.bfs_and_dfs;

public class Stack {
    private StackNode top;
    private int stackSize = 0;

    public void stackPush(int value){
        StackNode newStackNode = new StackNode(value);

        if(top == null){
            top = newStackNode;
            stackSize++;
        }else{
            newStackNode.setNextTop(top);
            top = newStackNode;
            stackSize++;
        }
    }

    public int stackPop(){
        StackNode temp = top;
        top = top.getNextTop();
        stackSize--;

        return temp.getValue();
    }

    public boolean empty(){
        if (top == null) return true;

        else return false;
    }
}
