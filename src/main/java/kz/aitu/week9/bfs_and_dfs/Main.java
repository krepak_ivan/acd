package kz.aitu.week9.bfs_and_dfs;

public class Main {
    public static void main(String[] args) {
        Tree tree_1 = new Tree();

        tree_1.insert(100);
        tree_1.insert(110);
        tree_1.insert(90);
        tree_1.insert(95);
        tree_1.insert(105);
        tree_1.insert(70);
        tree_1.insert(65);
        tree_1.insert(130);
        tree_1.insert(80);
        tree_1.insert(106);
        tree_1.insert(20);
        tree_1.insert(104);
        tree_1.insert(87);
        tree_1.insert(71);
        tree_1.insert(121);

        System.out.println("Printing [100 90 110 70 95 105 130 65 80 104 106 121 20 71 87]");
        tree_1.BFS();

        System.out.println();
        System.out.println("Printing [100 90 70 65 20 80 71 87 95 110 105 104 106 130 121]");
        tree_1.DFS();
    }
}