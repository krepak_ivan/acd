package kz.aitu.week9.bfs_and_dfs;

public class QueueNode {
    private int key;
    private QueueNode next;

    public QueueNode(int key){
        this.key = key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public int getKey() {
        return key;
    }

    public void setNext(QueueNode next) {
        this.next = next;
    }

    public QueueNode getNext() {
        return next;
    }
}
