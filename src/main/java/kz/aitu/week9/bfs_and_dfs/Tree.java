package kz.aitu.week9.bfs_and_dfs;

public class Tree {
    private Node root;

    public void insert(int key){
        root = insert(root, key);
    }

    private Node insert(Node node, int key){
        if (node == null) return new Node(key);

        int temp =  key - node.getKey();

        if(temp < 0) node.setLeft(insert(node.getLeft(), key));
        else if(temp > 0) node.setRight(insert(node.getRight(), key));
        else if(temp == 0) node.setKey(key);

        return node;
    }

    public void BFS(){
        Queue queue = new Queue();
        queue.push(root.getKey());

        print(root, queue);
    }

    private void print(Node root, Queue queue){
        while(queue.isEmpty() == false){
            Node current = root;
            int temp = printQueue(queue);

            while(current != null){
                if(temp < current.getKey()) current = current.getLeft();
                else if(temp > current.getKey()) current = current.getRight();
                else break;
            }

            if(current.getLeft() != null) queue.push(current.getLeft().getKey());
            if(current.getRight() != null) queue.push(current.getRight().getKey());
        }
    }

    public void DFS(){
        Stack stack = new Stack();
        stack.stackPush(root.getKey());

        print2(root, stack);
    }

    private void print2(Node root, Stack stack){
        while(stack.empty() == false){
            Node current = root;
            int temp = printStack(stack);

            while(current != null){
                if(temp < current.getKey()) current = current.getLeft();
                else if(temp > current.getKey()) current = current.getRight();
                else break;
            }

            if (current.getRight() != null) stack.stackPush(current.getRight().getKey());
            if (current.getLeft() != null) stack.stackPush(current.getLeft().getKey());
        }
    }

    private int printQueue(Queue queue){
        Node current = root;
        int subRoot = queue.pop();

        while (current!=null){
            if(subRoot < current.getKey()) current = current.getLeft();
            else if(subRoot > current.getKey()) current = current.getRight();
            else{
                System.out.print(current.getKey() + " ");
                break;
            }
        }

        return subRoot;
    }

    private int printStack(Stack stack){
        Node current = root;
        int subRoot = stack.stackPop();

        while(current!=null){
            if(subRoot < current.getKey()) current = current.getLeft();
            else if(subRoot > current.getKey()) current = current.getRight();
            else{
                System.out.print(current.getKey() + " ");
                break;
            }
        }

        return subRoot;
    }
}
