package kz.aitu.week9.bfs_and_dfs;

public class StackNode {
    private int value;
    private StackNode next;

    public StackNode(int value) {
        this.value = value;
    }

    public void setNextTop(StackNode nextTop) {
        this.next = nextTop;
    }

    public int getValue() {
        return value;
    }

    public StackNode getNextTop() {
        return next;
    }
}
