package kz.aitu.week2;

import java.util.Scanner;

public class LettersPermutations {
    public static void main(String[] args){
        String letters;
        Scanner input = new Scanner(System.in);
        letters = input.nextLine();
        int len = letters.length();
        recursionFunction(letters, 0, len-1);
    }

    static void recursionFunction(String letters, int a, int b){
        if(a == b) System.out.println(letters);
        else{
            for(int i = a; i <= b; i++){
                letters = swap(letters, a, i);
                recursionFunction(letters, a+1, b);
                letters = swap(letters, a, i);
            }
        }
    }

    public static String swap(String newWords, int i, int j){
        char temp;
        char[] lettersChar = newWords.toCharArray();

        temp = lettersChar[i] ;
        lettersChar[i] = lettersChar[j];
        lettersChar[j] = temp;

        return String.valueOf(lettersChar);
    }
}