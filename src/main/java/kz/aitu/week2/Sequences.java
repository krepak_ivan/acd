package kz.aitu.week2;

import java.util.Scanner;

public class Sequences {
    public static void main(String[] args) {
        int n;
        int k;
        Scanner input = new Scanner(System.in);
        n = input.nextInt();
        k = input.nextInt();
        int[] myArray = new int[k];

        recursionFunction(myArray, n, k, 0);
    }
    static void recursionFunction(int[] myArray, int n, int k, int q){
        int i;
        if(k == 0){
            for(i = 0; i < q; i++){
                System.out.print(myArray[i] + " ");
            }
            System.out.print("\n");
        }
        if(k > 0){
            for(i = 1; i <= n; i++){
                myArray[q] = i;
                recursionFunction(myArray, n, k-1, q+1);
            }
        }
    }
}