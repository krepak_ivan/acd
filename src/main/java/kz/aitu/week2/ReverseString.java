package kz.aitu.week2;

import java.util.Scanner;

public class ReverseString {
    public static void main(String[] args) {
       Scanner input = new Scanner(System.in);
       int n = input.nextInt();

       reverseStrings(n);
    }

    static void reverseStrings(int n){
        if(n > 0){
            Scanner inputString = new Scanner(System.in);
            String word = inputString.nextLine();
            reverseStrings(n-1);
            System.out.println(word);
        }
        else return;
    }
}