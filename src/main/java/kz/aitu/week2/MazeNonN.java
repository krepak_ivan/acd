package kz.aitu.week2;

import java.util.Scanner;

public class MazeNonN {
    private static int n;
    private static int m;
    private static int ans;
    private static char[][] a = new char[55][55];
    private static boolean[][] used = new boolean[55][55];

    public static void Main(String[] args){
        int i, j, c, d;
        Scanner input = new Scanner(System.in);
        n = input.nextInt();

        for (i = 0; i<n; i++){
            //a[i] = ConsoleInput.readToWhiteSpace(true).charAt(0);
            //char a[i] = input.next().charAt(0);
            char[] a = input.next().toCharArray();
        }

        c = input.nextInt();
        d = input.nextInt();
        rec(c - 1, d - 1);
        System.out.print(ans);
        System.out.print("\n");
        return;
    }

    private static void rec(int x,int y){
        if (used[x][y]) return;

        ans++;
        used[x][y] = true;

        if (a[x + 1][y] == '.') rec(x + 1, y);
        if (a[x - 1][y] == '.') rec(x - 1, y);
        if (a[x][y + 1] == '.'){
            rec(x, y + 1);
        }
        if (a[x][y - 1] == '.'){
            rec(x, y - 1);
        }
    }
}