package kz.aitu.week2;

import java.util.Scanner;

public class ExitFromTheMaze {
    private static int n;
    public static int m;
    public static int ans;
    private static char[][] a = new char[55][55];
    private static boolean[][] used = new boolean[55][55];

    public static void main(String[] args) {
        int i;
        Scanner input = new Scanner(System.in);
        n = input.nextInt();
        m = input.nextInt();

        for(i = 0; i < n; i++){
            a[i] = input.next().toCharArray();
        }

        rec(0, 0);

        if (ans != 0) System.out.print("YES");
        else System.out.print("NO");
    }

    private static void rec(int x, int y){
        if (used[x][y]) return;
        if (x == n - 1 && y == m - 1) ans = 1;

        used[x][y] = true;

        if (a[x + 1][y] == '.') rec(x + 1, y);
        if (a[x - 1][y] == '.') rec(x - 1, y);
        if (a[x][y + 1] == '.') rec(x, y + 1);
        if (a[x][y - 1] == '.') rec(x, y - 1);
    }
}