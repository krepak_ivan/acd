
import java.util.Scanner;

public class SquareTask {
    public static void main(String[] args) {
        int n;
        Scanner input = new Scanner(System.in);
        n = input.nextInt();

        int[][] myArray = new int[n][n];
        int q = 1;
        int low = 0;
        int high = n - 1;
        int count = ((n + 1)/2);

        for(int i = 0; i < count; i++){
            for(int j = low; j < high + 1; j++){
                myArray[i][j] = q;
                q++;
            }
            for(int j = low + 1; j < high + 1; j++){
                myArray[j][high] = q;
                q++;
            }
            for(int j = high - 1; j > low - 1; j--){
                myArray[high][j] = q;
                q++;
            }
            for(int j = high - 1; j > low; j--){
                myArray[j][low] = q;
                q++;
            }
            low++;
            high--;
        }

        //For output final array
        for(int i = 0; i<n; i++){
            for(int j = 0; j<n; j++){
                System.out.print(myArray[i][j] + " ");
            }
            System.out.print("\n");
        }
    }
}