package kz.aitu.week2;

import java.util.Scanner;

public class Task1Fibonnaci {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        System.out.println(fib(n));
    }

    public static int fib(int n){
        if(n == 0 || n == 1) return n;
        return fib(n-1) + fib(n-2);
    }
}
