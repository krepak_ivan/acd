package kz.aitu.week2;

import java.util.Scanner;

public class ReverseOrder {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int n;
        n = input.nextInt();

        System.out.println(reverseFunction(n));
    }

    public static int reverseFunction(int n){
        Scanner newScanner = new Scanner(System.in);
        int m = newScanner.nextInt();

        if(n == 1) return m;

        System.out.println(reverseFunction(n-1));
        return m;
    }
}

