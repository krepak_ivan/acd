package kz.aitu.linkedlistquiz2;

public class LinkedListTest {

    public static void main(String[] args) {
        LinkedList ls = new LinkedList();
        Node head = ls.head();



        ls.add(new Node("1"));//0
        ls.add(new Node("2"));//1
        ls.add(new Node("3"));//3
        ls.add(new Node("4"));//4
        ls.add(new Node("5"));//5 - remove
        ls.add(new Node("6"));//6 - remove
        ls.add(new Node("7"));//7

        ls.insertAt("cs1902", 2);
        ls.removeAt(5);
        ls.removeAt(6);

        Node a = ls.head();
        for(int q = 0; q < 7; q++){
            System.out.print(a + " ");
            a = a.next;
        }
    }

}
