package kz.aitu.linkedlistquiz2;

public class LinkedList {
    public Node head;
    public Node tail;

    public LinkedList(){
        this.head = new Node("head");
        tail = head;
    }

    public Node head() {
        return head;
    }

    public void add(Node node) {
        tail.next = node;
        tail = node;
    }

    public void insertAt(String s, int index) {
        Node temp = head;
        Node temp2 = new Node(s);
        int i = 0;

        while (i != index) {
            temp = temp.next;
            i++;
        }

        temp2.next = temp.next;
        temp.next = temp2;
    }

    public void removeAt(int index) {
        Node current = head;
        int i = 0;

        while (i < index) {
            current = current.next;
            i++;
        }

        Node deletedNode = current.next;
        current.next = deletedNode.next;
    }
}