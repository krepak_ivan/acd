package kz.aitu.merge3stacks;

import java.util.*;

public class Main {
    static Stack bigStack = new Stack();

    public static void main(String args[]) {
        Stack s1 = new Stack();
        Stack s2 = new Stack();
        Stack s3 = new Stack();

        s1.add(20);
        s1.add(19);
        s1.add(18);
        s1.add(17);
        s1.add(2);
        s1.add(1);
        s1.add(4);

        s2.add(18);
        s2.add(15);
        s2.add(10);
        s2.add(3);
        s2.add(4);
        s2.add(4);
        s2.add(17);

        s3.add(40);
        s3.add(35);
        s3.add(30);
        s3.add(20);
        s3.add(5);
        s3.add(6);
        s3.add(10);

        merege(s1, s2, s3);

        System.out.print("4th stack: ");
        printStack(bigStack);

        Stack<Integer> tmpStack = sortStack(bigStack);
        Stack<Integer> clearedStack = sortStack(bigStack);
        System.out.println();
        System.out.print("Sorted numbers are: ");
        printStack(tmpStack);

        System.out.println();
        printStack(clearedStack);
    }

    public static void merege(Stack s1, Stack s2, Stack s3) {
        while (s1.size() != 0) {
            bigStack.push(s1.peek());
            s1.pop();
        }

        while (s2.size() != 0) {
            bigStack.push(s2.peek());
            s2.pop();
        }

        while (s3.size() != 0){
            bigStack.push(s3.peek());
            s3.pop();
        }
    }

    public static Stack<Integer> sortStack(Stack<Integer> bigStack){
        Stack<Integer> tempStack = new Stack<Integer>();
        while(!bigStack.isEmpty()){
            int temp = bigStack.pop();
            while(!tempStack.isEmpty() && tempStack.peek() > temp){
                bigStack.push(tempStack.pop());
            }

            tempStack.push(temp);
        }

        Stack<Integer> tempStackNew = new Stack<Integer>();
        while(!tempStack.isEmpty()){
            int temp = tempStack.pop();
            while(!tempStackNew.isEmpty() && tempStackNew.peek() == temp){
                tempStackNew.pop();
                tempStackNew.push(0);
            }

            tempStackNew.push(temp);
        }

        return tempStackNew;
    }

    public static void printStack(Stack<Integer> s){
        Stack<Integer> copiedStack = (Stack<Integer>)s.clone();

        if(copiedStack.empty()){
            return;
        }

        int a = s.pop();
        if(a != 0 ){
        System.out.print(copiedStack.pop() + " ");
        printStack(copiedStack);
        }else{
            copiedStack.pop();
            printStack(copiedStack);
        }
    }
}